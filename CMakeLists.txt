#@author     : Zhu Fei
#@date       : 2023-03-23
#@description: Project-level CMakeLists.txt for building Physika
#@version    : 1.0

cmake_minimum_required(VERSION 3.11.4)

project(physika)

option(BUILD_SHARED_LIBS "Build as shared lib" ON)

set(SRC_ROOT             ${CMAKE_SOURCE_DIR}/src)
set(EXAMPLE_ROOT         ${CMAKE_SOURCE_DIR}/examples)
set(TEST_ROOT            ${CMAKE_SOURCE_DIR}/tests)
set(THIRD_PATH           ${CMAKE_SOURCE_DIR}/thirdparty)
set(INSTALL_PATH         ${CMAKE_INSTALL_PREFIX})
set(THIRD_INSTALL_PATH   ${INSTALL_PATH}/thirdparty)
set(EXAMPLE_INSTALL_PATH ${INSTALL_PATH}/bin/examples)
set(TEST_INSTALL_PATH    ${INSTALL_PATH}/bin/tests)
set(HEADER_INSTALL_PATH  ${INSTALL_PATH}/include)
set(LIB_INSTALL_PATH     ${INSTALL_PATH}/lib)


set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
if(CMAKE_SYSTEM_NAME STREQUAL "Linux" OR CMAKE_SYSTEM_NAME STREQUAL "Darwin")
    set(HOST_FLAGS "-O3")
    if(CMAKE_BUILD_TYPE STREQUAL Debug)
        set(HOST_FLAGS "-g")
    endif()
    set(HOST_FLAGS          "${HOST_FLAGS} -Wall -Wextra -Wfatal-errors -fexceptions -fPIC")
elseif(MSVC)
    set(HOST_FLAGS "-O2")
    if(CMAKE_BUILD_TYPE STREQUAL Debug)
        set(HOST_FLAGS "-DEBUG")
    endif()
    set(HOST_FLAGS "${HOST_FLAGS} -W4 -utf-8 -EHa")
    set(CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS ON) # TODO(ZhuFei): will export APIs only in the future
endif()
set(CUDA_FLAGS          "${HOST_FLAGS} -forward-unknown-to-host-compiler --extended-lambda --relocatable-device-code=true -Xcompiler")
set(CMAKE_C_FLAGS       "${HOST_FLAGS} ${CMAKE_C_FLAGS}")
set(CMAKE_CXX_FLAGS     "${HOST_FLAGS} ${CMAKE_CXX_FLAGS}")

# build library
add_subdirectory(${SRC_ROOT})

# build example
add_subdirectory(${EXAMPLE_ROOT})

# build test
enable_testing()
add_subdirectory(${TEST_ROOT})